<?php
/**
 * libFSImage is a monolithic php library to allow a simple yet powerful image manipulation API that abstracts away from the various backends that can power it.
 * 
 * This class is the entry point to use the libFSImage manipulation library.
 * 
 * Engines are automatically (or manually) loaded for the backend (e.g. gd, imagick, etc)
 *
 * 
 * @category  fusionLibs
 * @package   libFSImage
 * @license   http://www.gnu.org/licenses/lgpl-2.1.html
 * @version   0.1.0
 * @since   2013-05-13
 * @updated 2013-05-13
 * @example <br />
 * &nbsp;&nbsp;&nbsp;&nbsp;<?php<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include("libfsimage.gd.php");<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include("libfsimage.imagick.php");<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//include("libfsimage.svg.php");<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include("libfsimage.php");<br />
 * <br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$inst = new libFSImage(); //or new libFSImage(false) to disable autoloading of engines<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;?><br />
 * @author    Shawn Seet <shawnseet@fusionstream.org>
 */

    class libFSImage {
        /**
         * An associative array containing specific options and settings for the backend engines.<br />
         * <br />
         * This is mostly used as a structure. It may be used as a structure to be populated or as a structure that contains settings/options.<br />
         * <br />
         * <ul>
         *  <li><i>bool</i> $default['<b>crop</b>']<br />
         *  <i>default: false</i><br />
         *  Default option to either resize or crop.<br /><br /></li>
         *  <li><i>int</i> $default['<b>cropx</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Default start position on the x axis to begin a crop. A blank string means the value will calculated dynamically so that the image is cropped with equal cropping on the left and right side.<br /><br /></li>
         *  <li><i>int</i> $default['<b>cropy</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Default start position on the y axis to begin a crop. A blank string means the value will calculated dynamically so that the image is cropped with equal cropping on the top and bottom.<br /><br /></li>
         *  <li><i>int</i> $default['<b>density</b>']<br />
         *  <i>default: 72</i><br />
         *  Default pixel density to save to, in units of ppi.<br /><br /></li>
         *  <li><i>int</i> $default['<b>depth</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Default pixel depth to save to. A blank string means "use original image depth".<br /><br /></li>
         *  <li><i>string</i> $default['<b>filename</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Default name of new image file. A blank string means "save over the original file".<br /><br /></li>
         *  <li><i>string</i> $default['<b>filepath</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Default path to save the image file. A blank string means "save in the same directory".<br /><br /></li>
         *  <li><i>bool</i> $default['<b>keepaspect</b>']<br />
         *  <i>default: true</i><br />
         *  Default setting of whether or not to keep the aspect ratio when resizing.<br /><br /></li>
         *  <li><i>bool</i> $default['<b>overflow</b>']<br />
         *  <i>default: false</i><br />
         *  When $default['keepaspect'] is false and when resizing, setting this option to true will allow a resized image to overflow up to one of the bounds. e.g. If this option is set to true, resizing an image of dimensions 100x50 to 10x10 will result in an image dimension of 20x10. If it were set to false, the image dimension would be 10x5.<br /><br /></li>
         *  <li><i>bool</i> $default['<b>overflowcrop</b>']<br />
         *  <i>default: false</i><br />
         *  When $default['overflow'] is true and where applicable, setting this option to true will crop any overflow.<br /><br /></li>
         *  <li><i>int</i> $default['<b>shrinkenlarge</b>'] <br />
         *  <i>default: 0</i><br />
         *  Default setting of whether to only shrink images, enlarge them, or both
         *      <ol>
         *          <li value="-1">Shrink Only</li>
         *          <li value="0">Both</li>
         *          <li value="1">Enlarge Only</li>
         *      </ol>
         *  <br /></li>
         *  <li><i>string</i> $default['<b>type</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Default mimetype to save the image file as. A blank string means "save as the original filetype".<br /><br /></li>
         * </ul>
         * <br />
         * Engine options: <br />
         * <br />
         * <ul>
         *  <li><i>array</i> $default['<b>engines</b>']<br />
         *  <i>default: <set on instantiation></i><br />
         *  Contains an array listing the available engines, in the form of "<NameOfClassInstance>" => new class();<br /><br /></li>
         *  <li><i>array</i> $default['<b>useengines</b>']<br />
         *  <i>default: <empty></i><br />
         *  If empty, it will follow the order they are stored in $default['engines']. If this variable contains an array in the form of array("<NameOfClassInstance>", "<NameOfClassInstance2>", "<NameOfClassInstance3>"), the engines and engine order here will take precedence.<br /><br /></li>
         *  <li><i>bool</i> $default['<b>failover</b>']<br />
         *  <i>default: true</i><br />
         *  Setting this option to true, will allow the next engine in line (see $default['engines'] or $default['useengines']) to work its magic if the current one fails.<br /><br /></li>
         * </ul>
         * <br />
         * Engine specific options:<br />
         * <br />
         * <ul>
         *  <li><i>array</i> $default['<b>engine</b>']<br />
         *  <i>default: <set on instantiation></i><br />
         *  Contains an array with engine specific options, in the form of "<NameOfClassInstance>" => array("<option>" => "<value>");<br /><br /></li>
         * </ul>
         * @var array
         */
        protected $default;
        
        /**
         * An associative array containing specific settings and values for a file. This is effectively a placeholder variable.<br />
         * <br />
         * This is mostly used as a structure. It may be used as a structure to be populated or as a structure that contains settings/options.<br />
         * <br />
         * <ul>
         *  <li><i>string</i> $default['<b>filepath</b>']<br />
         *  <i>default: <blank string></i><br />
         *  File location. When used as a settings structure, a blank string means "follow the value in $default".<br /><br /></li>
         *  <li><i>string</i> $default['<b>width</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Image Width. When used as a settings structure, a blank string means "use the same width".<br /><br /></li>
         *  <li><i>string</i> $default['<b>height</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Image Height. When used as a settings structure, a blank string means "use the same height".<br /><br /></li>
         *  <li><i>string</i> $default['<b>density</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Pixel density of the image in units of ppi. When used as a settings structure, a blank string means "follow the value in $default".<br /><br /></li>
         *  <li><i>string</i> $default['<b>type</b>']<br />
         *  <i>default: <blank string></i><br />
         *  Mimetype of image file. When used as a settings structure, a blank string means "follow the value in $default".<br /><br /></li>
         * </ul>
         * @var array
         */
        protected $deffile;
        
        /**
         * An associative array containing error messages. <br />
         * <br />
         * This will be populated with the last error.<br />
         * <br />
         * <ul>
         *  <li><i>string</i> $error['<b>engine</b>']<br />
         *  <i>default: <blank string></i><br />
         *  The engine where the error occured.<br /><br /></li>
         *  <li><i>string</i> $error['<b>function</b>']<br />
         *  <i>default: <blank string></i><br />
         *  This is the function where the error occured.<br /><br /></li>
         *  <li><i>int</i> $error['<b>errnum</b>']<br />
         *  <i>default: <blank string></i><br />
         *  This is the error number associated with the error.<br /><br /></li>
         *  <li><i>string</i> $error['<b>errstr</b>']<br />
         *  <i>default: <blank string></i><br />
         *  This is a short bit of descriptive text related to the error.<br /><br /></li>
         * </ul>
         * The default error numbers and textual descriptions are as follows:<br />
         * <ul>
         *  <li><b>-1</b><br />
         *  Engine does not support this function.<br /></li>
         *  <li><b>-100</b><br />
         *  Fatal Error. Cannot read file. (e.g. File missing, wrong path, wrong permissions, etc)<br /></li>
         *  <li><b>-200</b><br />
         *  Fatal Error. Out of resources. (e.g. memory, timeout, etc)<br /></li>
         *  <li><b>-300</b><br />
         *  Fatal Error.<br /></li>
         * </ul>
         * <br />
         * @var array
         */
        protected $error;
        
        /**
         * Returns an array where appropriate default settings and options are overridden with user defined ones.
         *
         * @param array The user settings array. Need not be complete.
         *
         * @return array
         *      Returns the merged array.<br/>
         */
        protected function tloadargs($args) {
            return array_merge($args, $this->default);
        }
        
        
        
        
        
        
        
        
        /**
         * Le Constructor.
         */
        public function __construct($autoload = true) {
            $default = array();
            //$default[''];
            
            if ($autoload) {
                foreach (get_declared_classes() as $value) {
                    $t_engineorderarray = array();
                    if (preg_match("/libFSImage_.*/", $value) === 1) {
                        $t_engineorderarray[] = array("weight" => $value::$weight, "classname" => $value);
                    }
                }
                
                function sortorder($first, $second) { return $second['weight'] - $first['weight']; }
                uasort ($t_engineorderarray, sortorder);

                foreach ($t_engineorderarray as $value) {
                    registerEngine($value['classname']);
                }
            }
        }
        
        
        /**
         * Le Destructor.
         */
        public function __destruct() {
            ;
        }
        
        
        /**
         * Registers a new image manipulation engine.
         *
         * @param string The class name to register.
         * @param string {optional) The name to register the class as. Leave blank or omit from argument to follow $classname.
         *
         * @return  int|bool
         *      true  : Success<br/>
         *      false : General Failure.<br/>
         *      -1    : Fatal Error. Invalid $classname<br />
         *      -2    : Fatal Error. Invalid $classASname<br/>
         *      -3    : Fatal Error. Class already registered. Try a different $classASname.<br/>
         */
        public function registerEngine($classname = "", $classASname = "") {
            
            $classname = trim($classname);
            $classASname = trim($classASname);
            
            if (preg_match("/[^a-zA-Z9-0_]/", $classname)) { return -1; }
            if (preg_match("/[^a-zA-Z9-0_]/", $classASname)) { return -2; }
            
            if ($classASname == "") { $classASname = $classname; }
            if (!array_key_exists($classASname, $this->default['engines'])) {
                //array_push($this->default['engines'], array("$classASname"=> new $classname()));
                //array_unshift($this->default['engines'], array("$classASname"=> new $classname()));
                $this->default['engines']['classASname'] = new $classname();
                return true;
            } else {
                return -3;
            }
            
            return false;
        }
        
        /**
         * Reorders the image engines.
         * 
         * If an engine in $engineorder is not available, it is ignored.
         * If an engine is not listed in $engineorder, it is appended to those that are.
         *
         * @param array An array of strings of the ClassInstanceNames in the new order
         *
         * @return  bool
         *      true  : Success<br/>
         *      false : General Failure.<br/>
         */
        public function reorderEngines($engineorder) {
            $t_enginearray = array();
            foreach($engineorder as $value) {
                if (array_key_exists($value, $this->default['engines'])) {
                    $t_enginearray[$value] = $this->default['engines'][$value];
                }
            }
            $this->default['engines'] = array_merge($t_enginearray, array_diff_assoc($this->default['engines'], $t_enginearray));
        }
        
        /**
         * Permanently overwrites default settings and options with user defined ones.
         *
         * @param array The user settings array. Need not be complete.
         *
         * @return int
         *      Returns the number of unchanged settings.<br/>
         * @since   version 0.1; 2013-05-13 (trunk)
         */
        public function loadargs($args) {
            $this->default = array_merge($args, $this->default);
            return (count($this->default) - count($args));
        }
        
        
        
        
        
        /**
         * Remove all exif metadata from file.
         *
         * @param array|string An associative array containing file information. See $deffile for structure. Alternatively, a string containing the path to the file.
         *
         * @return bool
         *      <ul>
         *          <li><b>true</b><br />Success.<br /></li>
         *          <li><b>false</b><br />Fail.<br /></li>
         *      </ul>
         */
        function clearexif($file) {
            
        }
        
        /**
         * Selectively remove exif metadata from file.
         *
         * @param array|string An associative array containing file information. See $deffile for structure. Alternatively, a string containing the path to the file.
         * @param array|string A normal array containing the relevant exif keys to remove/empty.
         *
         * @return bool
         *      <ul>
         *          <li><b>true</b><br />Success.<br /></li>
         *          <li><b>false</b><br />Fail.<br /></li>
         *      </ul>
         */
        function removeexif($file, $arrayOfKeys) {
            
        }
        
        /**
         * Update exif metadata from file.
         *
         * @param array|string An associative array containing file information. See $deffile for structure. Alternatively, a string containing the path to the file.
         * @param array|string An associative array containing exif data in the form of key=>value
         *
         * @return bool
         *      <ul>
         *          <li><b>true</b><br />Success.<br /></li>
         *          <li><b>false</b><br />Fail.<br /></li>
         *      </ul>
         */
        function updateexif() {
            
        }
        
        /**
         * Returns exif metadata from file.
         *
         * @param array|string An associative array containing file information. See $deffile for structure. Alternatively, a string containing the path to the file.
         *
         * @return object
         *      Returns an object with exif data as key=>value
         */
        function viewexif() {
            
        }
        
        
        
        
        
        /**
         * Resize an image. <br />
         *
         * @param int The new width. If $height is 0, $width cannot be 0 and the aspect ratio is maintained based on $width.
         * @param int The new height. If $width is 0, $height cannot be 0 and the aspect ratio is maintained based on $height.
         * @param array|string An associative array containing file information. See $deffile for structure. Alternatively, a string containing the path to the file.
         * @param array {optional) Associative Array. This will temporarily override any settings in $default, in the scope of this function.
         * @param array {optional) Associative Array. This will temporarily override any settings in $default, in the scope of this function.
         * <br />
         * @return int|bool
         *      <ul>
         *          <li><b>true</b><br />
         *          Success. If &$newfile is provided, &$newfile will be updated with the details of the new file in the structure of $deffile.<br /></li>
         *          <li><b>false</b><br />
         *          General Error.<br /></li>
         *      </ul>
         * @error On error, it will populate $error with the predefined erorr numbers and text (or with the following)
         *      <ul>
         *          <li><b>None</b></li>
         *      </ul>
         * @todo    file path will check if the path is valid. At the same time, will accept non local files if able.<br />
         */
        public function resize($file, $width, $height, $args = "", &$newfile = "") {
            if (@!empty($args) && !isset($args)) { $args = tloadargs($args); }
            
            if (@!isset($arg['keepaspect']) && (@$arg['keepaspect'] != true)) {
                if ($width == 0 xor $height == 0) {
                    $arg['keepaspect'] = true;
                }
            }
            
            if (strpos($width, "%") !== FALSE) {
            
            }
            
            if (strpos($height, "%") !== FALSE) {
            
            }
        }
        
        public function resample() {
            
        }
        
        public function crush($file) {
            
        }
       
        public function crop() {
            
        }
        
        public function uncrop() {
            
        }
        
        public function rotate($file, $deg, $args = "", &$newfile = "") { //set overflow to true to not crop during rotation
            
        }
        
        
        
        public function watermark($file, $watermark) {
            
        }
        
        public function write() {
            
        }
        
        public function addFX() {
            
        }
    
    
    }
?>